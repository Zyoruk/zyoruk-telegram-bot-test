const Bot = require('node-telegram-bot-api');
const emoji = require('node-emoji');
const botController = require('./botController');

const token = process.env.TOKEN;
const botCI = new botController();


let bot;
if (process.env.NODE_ENV === 'production') {
	bot = new Bot(token);
	bot.setWebHook(process.env.HEROKU_URL + bot.token);
} else {
	bot = new Bot(token, { polling: true });
}

const showError = (error, chatId) => { 
	console.error({ error });
	bot.sendMessage(
		chatId,
		`${emoji.get('negative_squared_cross_mark')} Error: ${error}`
	);
}

console.info('Bot server started in the ' + process.env.NODE_ENV + ' mode');

// for message structure https://core.telegram.org/bots/api#message
// Match #1234 strings.
bot.onText(/(feat|fix|release)\[\#[0-9]*\]/i, (msg, match) => {
	const mr = msg;
	const mt = match;
	botCI.getProjects().then(projects => {  
		try {
			let foundOne = false;
			projects.forEach(project => {
				botCI.getCommits(project.id, mt[0]).then(commits => {
					if (commits.length > 0) {
						if (!foundOne) foundOne = true;
						commits.forEach(commit => {
							const message = `${emoji.get('white_check_mark')} Commit [${commit.short_id}] done by 
								${commit.committer_name} on ${commit.committed_date}.\n${commit.message}`;
							bot.sendMessage(
								mr.chat.id,
								message
							);
						});
					} else { 
						if (!foundOne) { 
							bot.sendMessage(
								mr.chat.id,
								`No commit with ${mt[0]} as message! for project ${project.name} ${emoji.get('open_mouth')} `
							);
						}
					}
				}).catch(err => showError(err, mr.chat.id,));
			});
		} catch (error) {
			showError(error, mr.chat.id);
		}
	}).catch(err => showError(err, mr.chat.id));
});

module.exports = bot;
