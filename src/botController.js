const request = require('request');
glToken = process.env.GITLAB_TOKEN;
user = process.env.USER;
apiBase = 'https://gitlab.com/api/v4';
defaultOptions = { url: apiBase, headers: { 'Private-Token': glToken } };

class BotController {
  getCommits(projectId, filter) { 
    return new Promise((resolve, reject) => { 
      request.get(
        `${defaultOptions.url}/projects/${projectId}/repository/commits`,
        { headers: defaultOptions.headers },
        (err, resp, body) => { 
          if (!err && resp.statusCode == 200) {
            resolve(filter ? 
              JSON.parse(body).filter(commit => commit.message.includes(filter)) :
              JSON.parse(body))
          } else { 
            reject(err);
          }
        });
    });
  }

  getProjects() { 
    return new Promise((resolve, reject) => { 
      request.get(
        `${defaultOptions.url}/users/${user}/projects?simple=true`,
        { headers: defaultOptions.headers },
        (err, resp, body) => {
          if (!err && resp.statusCode === 200) { 
            resolve(JSON.parse(body));
          } else { 
            reject(err);
          }
        });
    })
  }
}


module.exports = BotController;