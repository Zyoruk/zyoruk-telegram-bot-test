# heroku-node-telegram-bot

## Try bot locally

### Prerequisites

-   Docker

### Step-by-step

1. Create your own bot using Telegram's [BotFather](https://core.telegram.org/bots#3-how-do-i-create-a-bot) and grab your TOKEN.
2. Clone or download and unpack this repo.
3. Go to the app's folder
4. Got to the devops folder
5. Run `docker-compose up`
6. After it says "Got you, your name!" to you, we can go to the next step😎

## Deploy your bot to the heroku

### Gitlab CI/CD

1.  Just add HEROKU_STAGING_API_KEY to Gitlab CI/CD variables. (Project > Settings > CI/CD > Variables)

### Local

1. Create the [Heroku account](https://heroku.com) and install the [Heroku Toolbelt](https://toolbelt.heroku.com/).
2. Login to your Heroku account using `heroku login`.
3. Go to the app's folder using `cd ~/heroku-node-telegram-bot`
4. Run `heroku create` to prepare the Heroku environment.
5. Run `heroku config:set TOKEN=SET HERE THE TOKEN YOU'VE GOT FROM THE BOTFATHER` and `heroku config:set HEROKU_URL=$(heroku info -s | grep web_url | cut -d= -f2)` to configure environment variables on the server.
6. Run `git add -A && git commit -m "Ready to run on heroku" && git push heroku master` to deploy your bot to the Heroku server.
7. Send smth to the bot to check out if it works ok.

### Visit the original project for more info

Good luck!
